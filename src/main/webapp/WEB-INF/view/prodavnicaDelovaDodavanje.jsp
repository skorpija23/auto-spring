<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Prodaja delova.Dodavanje</title>
<link rel="stylesheet" href="../homaPage.css" />

<link
	href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css"
	rel="stylesheet"
	integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1"
	crossorigin="anonymous">
</head>
<body>
	<header>
		
		<div class="menu">
			<a href="prodavnicaDelovaKuca"><button class="myButton">Kuca</button></a>
			
			</a> <a href="prodavnicaDelovaPretraga"><button class="myButton">Pretraga
				</button></a> 
				<a href="prodavnicaDelovaFirme"><button class="myButton">Firme</button></a>
				 <a href="prodavnicaDelovaOnama"><button class="myButton">O
					nama</button></a>
				 <a href="prodavnicaDelovaDodavanje"><button
					class="activeButton">Dodavanje artikla</button></a>
					<form action="/app/logout" method="post">
					<button type="submit" class="myButton">Log out</button>
                     </form> 
		</div>
	</header>
	<div class="page">
		<form  style="font-family:Apple Chancery, cursive !important; padding-left: 300px !important; font-weight: bold !important;" 
		action="/app/prodavnicaDelova/prodavnicaDelovaDodavanje" method="post" enctype="multipart/form-data">
			<br>
				<p >Odaberite marku vozila:</p> 
			<select name="idAutoMarka" class="form-select" >
			<option value="">--Molimo vas odaberite marku vozila--</option>
				<c:forEach items="${listaMarki}" var="lmr" >
					<option value="${lmr.idAutoMarka}">${lmr.naziv }</option>
				</c:forEach>
			</select>
			
			<p >Odaberite model vozila:</p> 
			<select name="idAutoModel" class="form-select" >
			<option value="">--Molimo vas odaberite model vozila--</option>
				<c:forEach items="${listaModela}" var="lm" >				
					<option value="${lm.idAutoModel}">${lm.naziv }</option>
				</c:forEach>
			</select> 
		 
			<p >Odaberite  kategoriju delova:</p> 
			<select name="idKategorija" class="form-select" >
				<option value="">--Molimo vas odaberite kategoriju delova--</option>
				<c:forEach items="${listaKategorija}" var="ka" >				
					<option value="${ka.idKategorija}">${ka.naziv }</option>
				</c:forEach>
			</select> 
			<p >Odaberite pod kategoriju delova:</p> 
			<select name="idPodKategorija" class="form-select" >
			   <option value="">--Molimo vas odaberite pod kategoriju delova--</option>
				<c:forEach items="${listaPodKategorija}" var="pk" >			
					<option value="${pk.idPodKategorija}">${pk.naziv }</option>
				</c:forEach>
			</select> 
			<p >Naziv artikla:</p> <input type="text"
				class="form-control" id="naziv"
				placeholder="Upisite naziv artikla" name="naziv">
				
				<p>Slika :</p> <input type="text"
				class="form-control" id="slika"
				placeholder="Upisite url slike" name="slika">
				
				<p>Opis :</p> <input type="text"
				class="form-control" id="opis"
				placeholder="Unesite kratak opis" name="opis">
				
				<p>Cena artikla:</p> <input type="number"
				class="form-control" id="cena"
				placeholder="Upisite cenu artikla" name="cena">
				
				<p>Upisite u procentima iznos pdv-a:</p> <input type="number"
				class="form-control" name="pdv" id="pdv"
				placeholder="Upisite koliki je pdv artikla." name="pdv">
				
				 <p>Postavite sliku artikla(png, jpg): </p>
                 <input type="file" class="form-control"
                 name="image" accept="image/png, image/jpeg" />
     
				
				<br><button class="btn btn-outline-secondary" type="submit">Dodaj</button>
 				
		</form>

	</div>
</body>
<!--<script src="../homePageJS.js"></script>-->
</body>
</html>