<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Prodaja delova.Lista artikala</title>
<link rel="stylesheet"  href="../homaPage.css"  />
<link rel="stylesheet"  href="../tableStyle.css"  />

<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" /> 
<link
	href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css"
	rel="stylesheet"
	integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1"
	crossorigin="anonymous">

</head>
<body>
<header>

<div class="menu">
   <a href="prodavnicaDelovaKuca" ><button class="myButton">Kuca</button> </a> 
   <a href="prodavnicaDelovaFirme"><button class="myButton" >Povratak</button></a>
    	<form action="/app/logout" method="post">
	        <button type="submit" class="myButton">Log out</button>
        </form> 
</div>
</header>
<div class="title1">
  <div class="panel-body table-responsive">
                    <table class="table">
                        <thead>
                            <tr>
                             
                                <th>Pun naziv</th>
                                <th>Opis</th>
                                <th>Cena</th>
                                <th>Pdv</th>
                                <th>Cena sa pdv-om</th>
                                <th>Slika artikla: </th>
                            </tr>
                        </thead>
                     <c:forEach items="${listaArtikala}" var="la">
                        <tbody>
                             
                              
                                <td>${la.naziv }</td>
                                <td>${la.opis }</td>
                                <td>${la.cena } eura</td>
                                <td>${la.pdv} %</td>
                                <td>${la.cenaPdv } eura</td>
                                
                               
                                <td>
                                    <ul class="action-list">
                                   <img src="data:image/jpg;base64,${la.slikaFile}" alt="Slika artikla"  style="width: 100px" >
                                    </ul>
                                </td>
                              
                        </tbody>
                        </c:forEach>
                    </table>

</div>
</body>
<!--<script src="../homePageJS.js"></script>-->
</body>
</html>