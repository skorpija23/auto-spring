<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Prodaja delova.Kuca</title>
<link rel="stylesheet"  href="../homaPage.css"  />
<link
	href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css"
	rel="stylesheet"
	integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1"
	crossorigin="anonymous">
</head>
<body>
<header>

<div class="menu">
  <a href="prodavnicaDelovaKuca" ><button class="myButton">Kuca</button> </a> 
   <a href="prodavnicaDelovaPretraga" ><button class="activeButton" >Pretraga </button></a>
   <a href="prodavnicaDelovaFirme"><button class="myButton" >Firme </button></a>
   <a href="prodavnicaDelovaOnama"><button class="myButton" >O nama </button></a>
    <a href="prodavnicaDelovaDodavanje"><button class="myButton" >Dodavanje artikla </button></a>
    	<form action="/app/logout" method="post">
					<button type="submit" class="myButton">Log out</button>
                     </form> 
</div>
</header>
<div class="page">
		<br>
		<h1 style="color:white !important">Filter za pretragu.</h1>
		<br>
		<form  style="font-family:Apple Chancery, cursive !important; padding-left: 300px !important; font-weight: bold !important;" 
		action="/app/prodavnicaDelova/prodavnicaDelovaPretraga" method="post">
			<br>
			
			
			<p >Odaberite model vozila:</p> 
			<select name="idAutoModel" class="form-select" >
					<option value="0">--Pretraga po odaberite modelu vozila--</option>
				<c:forEach items="${listaModela}" var="lm" >			
					<option value="${lm.idAutoModel}">${lm.naziv }</option>
				</c:forEach>
			</select> 
		 
		
			<p >Odaberite pod kategoriju delova:</p> 
			<select name="idPodKategorija" class="form-select" >
			    	<option value="0">--Pretraga po pod kategoriji delova--</option>
				<c:forEach items="${listaPodKategorija}" var="pk" >			
					<option value="${pk.idPodKategorija}">${pk.naziv }</option>
				</c:forEach>
			</select> 
			<p >Upisite naziv artikla kojeg trazite:</p> <input type="text"
				class="form-control" id="naziv"
				placeholder="Upisite naziv artikla" name="naziv">
             <br><p >Ako ste izvrsili odabir svih parametara pritisnite dugme za pretragu.</p>
             
             <br><button class="btn btn-outline-secondary" type="submit">Pretraga</button>
</form>
</div>


</body>
<!--<script src="../homePageJS.js"></script>-->
</body>
</html>