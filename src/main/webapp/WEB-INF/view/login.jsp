<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
<link rel="stylesheet" href="./logInCss.css" />
</head>
<body>
	<div class="login-wrap">
		
		<div class="login-html">
			<input id="tab-1" type="radio" name="tab" class="sign-in" checked><label
				for="tab-1" class="tab">Uloguj se</label> <input id="tab-2"
				type="radio" name="tab" class="sign-up"><label for="tab-2"
				class="tab">Registracija</label>
			<div class="login-form">
				
					<div class="sign-in-htm">
					<form action="/app/login" method="post">
						<div class="alert alert-error">Pogresna lozinka</div>
						<div class="alert alert-success">Izlogovani ste.</div>
						<div class="group">
							<label for="username" class="label">Korisnicko ime</label> <input
								id="username" type="text" class="input" name="username">
						</div>
						<div class="group">
							<label for="password" class="label">Sifra</label> <input
								id="password" type="password" class="input" data-type="password"
								name="password">
						</div>
						<div class="group">
							<button type="submit" class="button">Uloguj me</button>
						</div>
					</form>

				</div>
		
			<form action="/app/prodavnicaDelova/registracija" method="post">
				<div class="sign-up-htm">
					<div class="group">
					<label for="naziv" class="label">Ime:</label>
					<input id="naziv" type="text" name="naziv" class="input">
				</div>
				<div class="group">
					<label for="pib" class="label">Pib:</label>
					<input id="pib" type="text" name="pib" class="input">
				</div>
				<div class="group">
					<label for="adresa" class="label">Adresa:</label>
					<input id="adresa" type="text" name="adresa" class="input">
				</div>
					<div class="group">
					<label for="telefon" class="label">Telefon:</label>
					<input id="telefon" type="text" name="telefon" class="input">
				</div>
					<div class="group">
					<label for="email" class="label">Email address:</label>
					<input id="email" type="email" name="email" class="input">
				</div>
					<div class="group">
					<label for="pass" class="label">Password</label>
					<input id="pass" type="password" name="pass"class="input" data-type="password">
				</div>
				<div class="group">
					<label for="username" class="label">Username</label>
					<input id="username" name="username" type="text" class="input">
				</div>
					<div class="group">
					<label for="oNama" class="label">O nama:</label>
					<input id="oNama" type="text" name="oNama" class="input">
				</div>
					<div class="group">
					<label for="slika" class="label">Slika:</label>
					<input id="slika" type="text" name="slika" class="input">
				</div>
					<div class="group">
							<button type="submit" class="button">Registruj se</button>
						</div>
				
				
				</div>
				</form>
				
			

				</div></div>
			</div>
		
	
</body>
</html>