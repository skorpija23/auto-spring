<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Prodaja delova.Kuca</title>
<link rel="stylesheet" href="../homaPage.css" />
<link rel="stylesheet" href="../tableStyle.css" />

<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" />
<link
	href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css"
	rel="stylesheet"
	integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1"
	crossorigin="anonymous">

</head>
<body>
	<header>

		<div class="menu">
			<a href="prodavnicaDelovaKuca"><button class="myButton">Kuca</button>
			</a> <a href="prodavnicaDelovaPretraga"><button class="myButton">Pretraga
				</button></a> <a href="prodavnicaDelovaFirme"><button class="activeButton">Firme
				</button></a> <a href="prodavnicaDelovaOnama"><button class="myButton">O
					nama</button></a> <a href="prodavnicaDelovaDodavanje"><button
					class="myButton">Dodavanje artikla</button></a>
			<form action="/app/logout" method="post">
				<button type="submit" class="myButton">Log out</button>
			</form>
		</div>
	</header>
	<br>
	<br>
	<div class="page">

		<div class="container">
			<div class="row">
				<div class="col-md-offset-1 col-md-10">
					<div class="panel">
						<div class="panel-heading">
							<div class="row">
								<div class="col col-sm-3 col-xs-12">
									<h4 class="title">
										Lista <span>Firmi</span>
									</h4>
								</div>
								<div class="col-sm-9 col-xs-12 text-right">
									<div class="btn_group">
										<input type="text" class="form-control" placeholder="Pretraga">
										<button class="btn btn-default" title="Reload">Pretrazi</button>

									</div>
								</div>
							</div>
						</div>
						<div class="panel-body table-responsive">
							<table class="table">
								<thead>
									<tr>

										<th>Pun naziv</th>
										<th>Pib</th>
										<th>Adresa</th>
										<th>E-mail</th>
										<th>Pozvati</th>
										<th>Lista artikala:</th>
										<th>Obim prodaje:</th>
									</tr>
								</thead>
								<c:forEach items="${listaProdavnica}" var="lp">

									<tbody>
										<tr>
											<td>${lp.naziv }</td>
											<td>${lp.pib }</td>
											<td>${lp.adresa }</td>
											<td>${lp.email }</td>
											<td>
												<ul class="action-list">
													<li><a href="tel:${lp.telefon }" data-tip="call"><i
															class="fa fa-phone"></i></a></li>
												</ul>
											</td>
											<td>
												<ul class="action-list">
													<form
														action="/app/prodavnicaDelova/prodavnicaListaArtikala"
														method="get">
														<input type="hidden" name="idProdavnica"
															value="${lp.idProdavnica }" />
														<button type="submit" class="btn btn-default">Pretrazi</button>
													</form>
												</ul>
											</td>
											<td><c:forEach items="${velicinaProdavnica}" var="vp">

													<c:if test="${ vp.idProdavnice == lp.idProdavnica}">
														<c:if test="${ vp.redosledNajveciKaNajmanjem == 1}">
															<img style="height: 40px;"
																src="https://thumbs.dreamstime.com/b/gold-medal-best-first-place-winner-champion-number-one-st-place-metalworker-s-reward-red-ribbon-isolated-white-gold-medal-best-158358278.jpg"
																alt="zlatna medalja">
														</c:if>
														<c:if test="${ vp.redosledNajveciKaNajmanjem == 2}">
															<img style="height: 40px;"
																src="https://thumbs.dreamstime.com/z/champion-silver-medal-red-ribbon-white-background-metallic-winner-award-second-place-nd-realistic-vector-180162505.jpg"
																alt="srebrna medalja">
														</c:if>
														<c:if test="${ vp.redosledNajveciKaNajmanjem == 3}">
															<img style="height: 40px;"
																src="https://previews.123rf.com/images/olegganko/olegganko1804/olegganko180400209/100578089-champion-art-bronze-medal-with-red-ribbon-icon-sign-first-place-isolated-on-transparent-background-v.jpg"
																alt="srebrna medalja">
														</c:if>
													</c:if>

												</c:forEach></td>


										</tr>
									</tbody>

								</c:forEach>
							</table>
						</div>
						<div class="panel-footer">
							<div class="row">
								<div class="col col-sm-6 col-xs-6">
									Lista nudi prikaz po <b>5</b> od svih <b></b> unosa.
								</div>
								<div class="col-sm-6 col-xs-6">
									<form action="/app/prodavnicaDelova/prodavnicaDelovaFirme"
										method="get">
										<label for="cars">Odaberite broj prikaza pro stranici:</label>

										<select name="brojPrikaza" id="brojPrikaza">

											<option value="1" ${brojPrikaza == 1 ? "selected" : ""}>1</option>
											<option value="5" ${brojPrikaza == 5 ? "selected" : ""}>5</option>
											<option value="10" ${brojPrikaza == 10 ? "selected" : ""}>10</option>


										</select> <input type="submit" value="Odaberi" />
									</form>

									<form action="/app/prodavnicaDelova/prodavnicaDelovaFirme"
										method="get">
										<nav aria-label="Navigation for countries">
											<ul class="pagination">
												<c:if test="${brojStranice != 1}">
													<li class="page-item"><a class="page-link"
														href="/app/prodavnicaDelova/prodavnicaDelovaFirme?brojStranice=${brojStranice - 1}&brojPrikaza=${brojPrikaza}">Nazad</a>
													</li>
												</c:if>

												<c:forEach begin="1" end="${ukupanBrojStranica }" var="i">
													<c:choose>
														<c:when test="${brojStranice eq i}">
															<li class="page-item active"><a class="page-link">
																	${i} <span class="sr-only">(current)</span>
															</a></li>
														</c:when>
														<c:otherwise>
															<li class="page-item"><a class="page-link"
																href="prodavnicaDelovaFirme?brojStranice=${i}&brojPrikaza=${brojPrikaza}">${i}</a>
															</li>
														</c:otherwise>
													</c:choose>
												</c:forEach>

												<c:if test="${brojStranice < ukupanBrojStranica}">
													<li class="page-item"><a class="page-link"
														href="/app/prodavnicaDelova/prodavnicaDelovaFirme?brojStranice=${brojStranice + 1}&brojPrikaza=${brojPrikaza}">Napred</a>
													</li>
												</c:if>
											</ul>
										</nav>


									</form>

								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>


</body>
<!--<script src="../homePageJS.js"></script>-->
</body>
</html>