package com.project.auto.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class LogInController {

	@GetMapping("/login")
	public String login(ModelMap model) {
		return "login";
	}
}