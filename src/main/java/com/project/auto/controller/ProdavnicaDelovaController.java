package com.project.auto.controller;

import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

import javax.imageio.ImageIO;

import org.apache.tomcat.util.codec.binary.Base64;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import com.project.auto.dto.ArtikalDTO;
import com.project.auto.dto.AutoMarkaDTO;
import com.project.auto.dto.AutoModelDTO;
import com.project.auto.dto.CreateArtikalDTO;
import com.project.auto.dto.CreateProdavnicaDTO;
import com.project.auto.dto.FilterArtiklaDTO;
import com.project.auto.dto.KategorijaDTO;
import com.project.auto.dto.PodKategorijaDTO;

import com.project.auto.dto.ProdavnicaDTO;

import com.project.auto.dto.ProdavniceSaBrojemArtikalaDTO;

import com.project.auto.model.Artikal;
import com.project.auto.model.Prodavnica;
import com.project.auto.service.ProdavnicaService;

//import sun.misc.BASE64Decoder;

@Controller
@RequestMapping("/prodavnicaDelova")
public class ProdavnicaDelovaController {

	@Autowired
	private ProdavnicaService prodavnicaService;

	@GetMapping("/autoDelovi")
	public ModelAndView getUser() {

		ModelAndView model = new ModelAndView("prodavnicaDelovaKuca");

		return model;
	}

	@PostMapping("/registracija")
	public String registracija(CreateProdavnicaDTO request) {

		prodavnicaService.createNewUser(request);
		return "redirect:app/login";
	}

	@GetMapping("/prodavnicaDelovaKuca")
	public ModelAndView prodavnicaDelovaKuca() {
		ModelAndView model = new ModelAndView("prodavnicaDelovaKuca");

		return model;
	}

	@GetMapping("/prodavnicaDelovaPretraga")
	public ModelAndView prodavnicaDelovaPretraga() {
		ModelAndView model = new ModelAndView("prodavnicaDelovaPretraga");

		List<AutoModelDTO> listaModela = prodavnicaService.findAllModel();

		model.addObject("listaModela", listaModela);
		List<AutoMarkaDTO> listaAutoMarki = prodavnicaService.findAllAutoMarka();
		model.addObject("listaMarki", listaAutoMarki);
		List<KategorijaDTO> listaKategorija = prodavnicaService.findAllKategorija();
		model.addObject("listaKategorija", listaKategorija);
		List<PodKategorijaDTO> listaPodKategorija = prodavnicaService.findAllPodKategorija();
		model.addObject("listaPodKategorija", listaPodKategorija);
		return model;
		
	}

	@PostMapping("/prodavnicaDelovaPretraga")
	public ModelAndView prodavnicaDelovaFilter(FilterArtiklaDTO request) throws UnsupportedEncodingException {
		ModelAndView model = new ModelAndView("listaArtikala");
		
		List<ArtikalDTO> listaArtikalaDTO=prodavnicaService.filterArtikala(request);

		
		model.addObject("listaArtikala",listaArtikalaDTO);
		return model;
		
	}
	
	@GetMapping("/prodavnicaDelovaFirme")
	public ModelAndView prodavnicaDelovaKontak(@RequestParam(value="brojStranice", defaultValue = "1") int brojStranice
			,@RequestParam(value="brojPrikaza", defaultValue ="5") int brojPrikaza) {
		ModelAndView model = new ModelAndView("prodavnicaDelovaFirme");
		System.out.println(brojPrikaza+" broj prodavnica");
		List<ProdavnicaDTO> listaProdavnicaDTO=prodavnicaService.findAllProdavnica(brojStranice,brojPrikaza);
	    List<Prodavnica>listaProdavnica=new ArrayList<Prodavnica>();
		int ukupanBrojStranica=0;
	    for(ProdavnicaDTO p: listaProdavnicaDTO) {
			listaProdavnica=p.getListaProdavnica();
			ukupanBrojStranica=p.getUkupanBrojStranica();
		}
	    model.addObject("brojPrikaza", brojPrikaza);
		model.addObject("brojStranice", brojStranice);
		model.addObject("listaProdavnica",listaProdavnica);

		System.out.println(ukupanBrojStranica+" ukupan broj kotroler");
		model.addObject("ukupanBrojStranica",ukupanBrojStranica);

		List<ProdavniceSaBrojemArtikalaDTO> listaRedosledaPoVelicini=prodavnicaService.findThreeBigMarket();
		System.out.println(listaRedosledaPoVelicini);
		model.addObject("velicinaProdavnica",listaRedosledaPoVelicini);

		return model;
	}

	@GetMapping("/prodavnicaDelovaOnama")
	public ModelAndView prodavnicaDelovaOnama() {
		ModelAndView model = new ModelAndView("prodavnicaDelovaOnama");
	
		return model;
	}

	@GetMapping("/prodavnicaDelovaDodavanje")
	public ModelAndView prodavnicaDelovaDodavanje() {
		ModelAndView model = new ModelAndView("prodavnicaDelovaDodavanje");
		List<AutoModelDTO> listaModela = prodavnicaService.findAllModel();

		model.addObject("listaModela", listaModela);
		List<AutoMarkaDTO> listaAutoMarki = prodavnicaService.findAllAutoMarka();
		model.addObject("listaMarki", listaAutoMarki);
		List<KategorijaDTO> listaKategorija = prodavnicaService.findAllKategorija();
		model.addObject("listaKategorija", listaKategorija);
		List<PodKategorijaDTO> listaPodKategorija = prodavnicaService.findAllPodKategorija();
		model.addObject("listaPodKategorija", listaPodKategorija);
		return model;
	}

	@PostMapping("/prodavnicaDelovaDodavanje")
	public ModelAndView dodavanjeArtikla(CreateArtikalDTO request, @RequestParam("image") MultipartFile multipartFile) throws IOException {
		org.springframework.security.core.Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		String name = auth.getName();
		request.setUsername(name);
		
		byte[] slika = multipartFile.getBytes();
	    request.setSlikaFile(slika);
		
		ModelAndView model = new ModelAndView("prodavnicaDelovaDodavanje");
		prodavnicaService.saveNewArtikal(request);
		
		return model;
	}

	@GetMapping("/artikal")
	public ModelAndView artikal() {
		ModelAndView model = new ModelAndView("prodavnicaDelovaDodavanje");

		return model;
	}

	@PostMapping("/artikal")
	public String artikal(CreateArtikalDTO request) {

		return null;

	}
	@GetMapping("/prodavnicaListaArtikala")
	public ModelAndView prodavnicaDelovaListaArtikala(@RequestParam("idProdavnica") int idProdavnica) throws IOException {
		ModelAndView model = new ModelAndView("listaArtikala");
		List<ArtikalDTO> listaArtikala=prodavnicaService.findArtikalById(idProdavnica);
		System.out.println("upao u kotroler");
		System.out.println(listaArtikala+" lista artikala po id-u ");
		
		model.addObject("listaArtikala",listaArtikala);
		
		return model;
	}
	
}
