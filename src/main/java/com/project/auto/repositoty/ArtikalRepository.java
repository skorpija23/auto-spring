package com.project.auto.repositoty;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.project.auto.model.Artikal;
import com.project.auto.model.Prodavnica;


@Repository
public interface ArtikalRepository extends JpaRepository<Artikal,Integer> {

	@Query(value = "Select a.id_artikal FROM artikal as a WHERE a.prodavnica_id_prodavnica = (?1) ", nativeQuery = true)
	List<Object[]> findArtikalById(int idProdavnica);

	List<Artikal> findById(int idArtikal);
	
	List<Artikal> findAllByProdavnica(Prodavnica prodavnica);
}
