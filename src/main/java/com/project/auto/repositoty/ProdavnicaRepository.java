package com.project.auto.repositoty;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;


import com.project.auto.model.Prodavnica;

@Repository
public interface ProdavnicaRepository extends JpaRepository<Prodavnica,Integer>{

	Prodavnica findByUsername(String username);

	@Query(value="SELECT a.prodavnica_id_prodavnica, COUNT(a.prodavnica_id_prodavnica) AS brojArtikala FROM artikal AS a GROUP BY a.prodavnica_id_prodavnica "
			+ "ORDER BY brojArtikala DESC LIMIT 3", nativeQuery= true)
	
	List<Object[]>  findThreeBigestMarket();
	
}



	