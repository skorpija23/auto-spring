package com.project.auto.repositoty;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.project.auto.model.AutoModel;
import com.project.auto.model.PodKategorija;

@Repository
public interface PodKategorijaRespository extends JpaRepository<PodKategorija,Integer> {

}
