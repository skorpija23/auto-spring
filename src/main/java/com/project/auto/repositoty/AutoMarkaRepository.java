package com.project.auto.repositoty;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.project.auto.model.AutoMarka;


@Repository
public interface AutoMarkaRepository extends JpaRepository<AutoMarka,Integer>{
	
	
}

