package com.project.auto.repositoty;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.project.auto.model.Kategorija;

@Repository
public interface KategorijaRepository extends JpaRepository<Kategorija,Integer>{

}
