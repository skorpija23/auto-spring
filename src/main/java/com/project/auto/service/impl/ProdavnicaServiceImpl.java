package com.project.auto.service.impl;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceUnit;

import org.apache.tomcat.util.codec.binary.Base64;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;


import com.project.auto.dto.ArtikalDTO;
import com.project.auto.dto.AutoMarkaDTO;
import com.project.auto.dto.AutoModelDTO;
import com.project.auto.dto.CreateArtikalDTO;
import com.project.auto.dto.CreateProdavnicaDTO;
import com.project.auto.dto.FilterArtiklaDTO;
import com.project.auto.dto.KategorijaDTO;
import com.project.auto.dto.PodKategorijaDTO;

import com.project.auto.dto.ProdavnicaDTO;

import com.project.auto.dto.ProdavniceSaBrojemArtikalaDTO;

import com.project.auto.model.Artikal;
import com.project.auto.model.AutoMarka;
import com.project.auto.model.AutoModel;
import com.project.auto.model.Kategorija;
import com.project.auto.model.PodKategorija;
import com.project.auto.model.Prodavnica;
import com.project.auto.repositoty.ArtikalRepository;
import com.project.auto.repositoty.AutoMarkaRepository;
import com.project.auto.repositoty.AutoModelRepository;
import com.project.auto.repositoty.KategorijaRepository;
import com.project.auto.repositoty.PodKategorijaRespository;
import com.project.auto.repositoty.ProdavnicaRepository;
import com.project.auto.service.ProdavnicaService;


@Service
public class ProdavnicaServiceImpl implements ProdavnicaService {
 
	
	@PersistenceUnit
	private EntityManagerFactory emf;
	@Autowired
	private BCryptPasswordEncoder bCryptPasswordEncoder;
	@Autowired
	private ProdavnicaRepository prodavnicaRepository;
	@Autowired
	private AutoModelRepository autoModelRepository;
	@Autowired
	private AutoMarkaRepository autoMarkaRepository;
	@Autowired
	private KategorijaRepository kategorijaRepository;
	@Autowired
	private PodKategorijaRespository podKategorijaRepository;
	@Autowired
	private ArtikalRepository artikalRepository;
    
	
	@Override
	public void createNewUser(CreateProdavnicaDTO request) {
		// TODO Auto-generated method stub
		Prodavnica prodavnica = new Prodavnica();
		prodavnica.setPass(bCryptPasswordEncoder.encode(request.getPass()));
		prodavnica.setNaziv(request.getNaziv());
		prodavnica.setPib(request.getPib());
		prodavnica.setAdresa(request.getAdresa());
		prodavnica.setUsername(request.getUsername());
		prodavnica.setoNama(request.getoNama());
		prodavnica.setSlika(request.getSlika());
		prodavnica.setTelefon(request.getTelefon());
		prodavnica.setEmail(request.getEmail());
		prodavnicaRepository.save(prodavnica);
	}

	@Override
	public List<AutoModelDTO> findAllModel() {
		// TODO Auto-generated method stub
		List<AutoModel> modeliAutomobila = autoModelRepository.findAll();
		List<AutoModelDTO> listaModela = new ArrayList<AutoModelDTO>();
		for (AutoModel am : modeliAutomobila) {
			AutoModelDTO tmpDTO = new AutoModelDTO();
			tmpDTO.setGodiste(am.getGodiste());
			tmpDTO.setNaziv(am.getNaziv());
			tmpDTO.setOznaka(am.getOznaka());
			tmpDTO.setIdAutoModel(am.getIdAutoModel());
			tmpDTO.setSlika(am.getSlika());
			listaModela.add(tmpDTO);

		}
		return listaModela;
	}

	@Override
	public List<AutoMarkaDTO> findAllAutoMarka() {
		// TODO Auto-generated method stub
		List<AutoMarka> markeAutomobila = autoMarkaRepository.findAll();
		List<AutoMarkaDTO> listaMarka = new ArrayList<AutoMarkaDTO>();
		for (AutoMarka am : markeAutomobila) {
			AutoMarkaDTO tmpDTO = new AutoMarkaDTO();
			tmpDTO.setIdAutoMarka(am.getIdAutoMarka());
			tmpDTO.setNaziv(am.getNaziv());
			tmpDTO.setSlika(am.getSlika());
			listaMarka.add(tmpDTO);
		}

		return listaMarka;

	}

	@Override
	public List<KategorijaDTO> findAllKategorija() {
		// TODO Auto-generated method stub
		List<Kategorija> kategorija = kategorijaRepository.findAll();
		List<KategorijaDTO> listaKategorija = new ArrayList<KategorijaDTO>();
		for (Kategorija k : kategorija) {
			KategorijaDTO tmpDTO = new KategorijaDTO();
			tmpDTO.setIdKategorija(k.getIdKategorija());
			tmpDTO.setNaziv(k.getNaziv());
			tmpDTO.setOpis(k.getOpis());
			tmpDTO.setSlika(k.getSlika());
			listaKategorija.add(tmpDTO);
		}
		return listaKategorija;
	}

	@Override
	public List<PodKategorijaDTO> findAllPodKategorija() {
		// TODO Auto-generated method stub
		List<PodKategorija> podKategorija = podKategorijaRepository.findAll();
		List<PodKategorijaDTO> listaPodKategorija = new ArrayList<PodKategorijaDTO>();
		for (PodKategorija pk : podKategorija) {
			PodKategorijaDTO tmpDTO = new PodKategorijaDTO();
			tmpDTO.setIdPodKategorija(pk.getIdPodKategorija());
			tmpDTO.setNaziv(pk.getNaziv());
			tmpDTO.setOpsi(pk.getOpsi());
			listaPodKategorija.add(tmpDTO);
		}

		return listaPodKategorija;
	}

	@Override
	public void saveNewArtikal(CreateArtikalDTO request) {
		// TODO Auto-generated method stub
		String username = request.getUsername();
		int idAutoModel = request.getIdAutoMarka();
		int idPodKategorija = request.getIdPodKategorija();
		String nazivArtikla = request.getNaziv();
		String opisArtikla = request.getOpis();
		String slika = request.getSlika();
		float cena = request.getCena();
		float pdv = request.getPdv();
		float pdvIznos = pdv / 100 + 1;
		float cenaPdv = cena * pdvIznos;
		byte[] slikaFile=request.getSlikaFile();
		Artikal noviArtikal = new Artikal();
		AutoModel autoModel = findModelAuto(idAutoModel);
		noviArtikal.setAutoModel(autoModel);
		PodKategorija podKategorija = findPodKategorija(idPodKategorija);
		Prodavnica prodavnica = prodavnicaRepository.findByUsername(username);
		noviArtikal.setProdavnica(prodavnica);
		noviArtikal.setPodKategorija(podKategorija);
		noviArtikal.setNaziv(nazivArtikla);
		noviArtikal.setOpis(opisArtikla);
		noviArtikal.setSlika(slika);
		noviArtikal.setCena(cena);
		noviArtikal.setPdv(pdv);
		noviArtikal.setCenaPdv(cenaPdv);
		noviArtikal.setSlikaFile(slikaFile);
		artikalRepository.save(noviArtikal);
	}

	private PodKategorija findPodKategorija(int idPodKategorija) {
		// TODO Auto-generated method stub
		Optional<PodKategorija> podKategorijaOpt = podKategorijaRepository.findById(idPodKategorija);
		if (podKategorijaOpt.isPresent()) {
			return podKategorijaOpt.get();
		}

		return null;
	}

	private AutoModel findModelAuto(int idAutoModel) {
		// TODO Auto-generated method stub
		Optional<AutoModel> autoModelOpt = autoModelRepository.findById(idAutoModel);
		if (autoModelOpt.isPresent()) {
			return autoModelOpt.get();
		}
		return null;
	}

	@Override
	public List<ProdavnicaDTO> findAllProdavnica(int brojStranice, int brojPrikaza) {
		brojStranice--;
		Pageable firstPageWithTwoElements = PageRequest.of(brojStranice, brojPrikaza);
		Page<Prodavnica> stranicaProdavnica =  prodavnicaRepository.findAll(firstPageWithTwoElements);
		int brojPage=stranicaProdavnica.getTotalPages();
	    
		List<Prodavnica> listaProdavnica = stranicaProdavnica.getContent();
		List<ProdavnicaDTO> listaProdavnicaDTO=new ArrayList<ProdavnicaDTO>();
		ProdavnicaDTO prodavnicaDTO=new ProdavnicaDTO();
		prodavnicaDTO.setListaProdavnica(listaProdavnica);
		prodavnicaDTO.setUkupanBrojStranica(brojPage);
		listaProdavnicaDTO.add(prodavnicaDTO);
		return listaProdavnicaDTO;
	}

	@Override
	public List<ArtikalDTO> filterArtikala(FilterArtiklaDTO request) throws UnsupportedEncodingException {
		// TODO Auto-generated method stub

		EntityManager em = emf.createEntityManager();
		
		StringBuilder sb=new StringBuilder();

		sb.append("SELECT  a.naziv, a.slika, a.opis, a.cena, a.pdv, a.cena_pdv, a.slika_file FROM artikal as a WHERE ");

		boolean prviFilter=true;
		
		Integer idAutoModel=request.getIdAutoModel();
		if(idAutoModel!=null && idAutoModel>0) {
			sb.append("a.auto_model_id_auto_model = '"+idAutoModel+"'");
			prviFilter=false;	
		}
		
		Integer idPodKategorija=request.getIdPodKategorija();
		if(idPodKategorija!=null && idPodKategorija>0) {
			if(prviFilter) {
				sb.append("a.pod_kategorija_id_pod_kategorija = '"+idPodKategorija+"'");
				prviFilter=false;
			}else {
				sb.append(" AND a.pod_kategorija_id_pod_kategorija = '"+idPodKategorija+"'");
			}
		}
		String naziv=request.getNaziv();
		if(naziv!=null && !naziv.isEmpty()) {
			if(prviFilter) {
				sb.append("a.naziv like '%"+naziv+"%'");
				prviFilter=false;
			}else {
			    sb.append(" AND a.naziv like '%"+naziv+"%'");
		    }
		}
		String sql="";
		if(!prviFilter) {
			sql=sb.toString();
		}else {

			sql="SELECT a.naziv, a.slika, a.opis, a.cena, a.pdv, a.cena_pdv,a.slika_file FROM artikal as a";

		}
		System.out.println(sql);
		List<Object[]> listaObjekata=em.createNativeQuery(sql).getResultList();
	    System.out.println(listaObjekata.toString()+" lista objekata iz servisa");
		List<ArtikalDTO> listaArtikala=new ArrayList<ArtikalDTO>();
		for(Object[] objekt: listaObjekata) {
	          String nazivArtikla=objekt[0].toString();
	          String slika=objekt[1].toString();
	          String opis=objekt[2].toString();
	          Float cena=Float.parseFloat(objekt[3].toString());
	          Float pdv=Float.parseFloat(objekt[4].toString());
	          Float cenaPdv=Float.parseFloat(objekt[5].toString());

	          byte[] slikaFile= (byte[]) (objekt[6]);

	          byte[] encodeBase64 = Base64.encodeBase64(slikaFile);
	      	
				
	         ArtikalDTO filtriraniArtikal=new ArtikalDTO();
	         filtriraniArtikal.setNaziv(nazivArtikla);
	         filtriraniArtikal.setSlika(slika);
	         filtriraniArtikal.setOpis(opis);
	         filtriraniArtikal.setCena(cena);
	         filtriraniArtikal.setPdv(pdv);
	         filtriraniArtikal.setCenaPdv(cenaPdv);	 	
			String base64Encoded=new String(encodeBase64, "UTF-8");
			filtriraniArtikal.setSlikaFile(base64Encoded);
	         listaArtikala.add(filtriraniArtikal);
		}
		System.out.println(listaArtikala+" lista artikala iz servisa");
		return listaArtikala;
	}

	@Override
	public List<ArtikalDTO> findArtikalById(int idProdavnica) throws UnsupportedEncodingException {
		// TODO Auto-generated method stub
		//*List<Object[]> listaObjekata=artikalRepository.findArtikalById(idProdavnica);
		//*List<ArtikalDTO> listaArtikala=new ArrayList<ArtikalDTO>();
//		for(Object[] objekt: listaObjekata) {
//	          String nazivArtikla=objekt[0].toString();
//	          String slika=objekt[1].toString();
//	          String opis=objekt[2].toString();
//	          Float cena=Float.parseFloat(objekt[3].toString());
//	          Float pdv=Float.parseFloat(objekt[4].toString());
//	          Float cenaPdv=Float.parseFloat(objekt[5].toString());
//	          byte[] slikaFile= SerializationUtils.serialize(objekt[6]);
//	          byte[] encodeBase64 = Base64.encodeBase64(slikaFile);
//	          System.out.println(slikaFile);
//	        	System.out.println(encodeBase64);	
//	         ArtikalDTO filtriraniArtikal=new ArtikalDTO();
//	         filtriraniArtikal.setNaziv(nazivArtikla);
//	         filtriraniArtikal.setSlika(slika);
//	         filtriraniArtikal.setOpis(opis);
//	         filtriraniArtikal.setCena(cena);
//	         filtriraniArtikal.setPdv(pdv);
//	         filtriraniArtikal.setCenaPdv(cenaPdv);
//	          filtriraniArtikal.setSlikaFile(encodeBase64);
//	         listaArtikala.add(filtriraniArtikal);
//		}
		//*int idArtikal=0;
		//*for(Object[] objekt: listaObjekata) {
		//*	idArtikal=Integer.parseInt(objekt[0].toString());
		//*	
		//*}
		Prodavnica prodavnica = prodavnicaRepository.findById(idProdavnica).get();
		List<Artikal> listaArtikala = artikalRepository.findAllByProdavnica(prodavnica);
		//*List<Artikal> listaArtikal=artikalRepository.findById(idArtikal);
		List<ArtikalDTO> listaArtikalaDTO=new ArrayList<ArtikalDTO>();
		for(Artikal a: listaArtikala) {
			ArtikalDTO artikalDTO=new ArtikalDTO();
			artikalDTO.setNaziv(a.getNaziv());
			artikalDTO.setOpis(a.getOpis());
			artikalDTO.setCena(a.getCena());
			artikalDTO.setCenaPdv(a.getCenaPdv());
			artikalDTO.setPdv(a.getPdv());
			artikalDTO.setSlika(a.getSlika());
			
			byte [] slikaUBajtu=a.getSlikaFile();		
			byte[] encodeBase64= Base64.encodeBase64(slikaUBajtu);
			String base64Encoded=new String(encodeBase64, "UTF-8");
			artikalDTO.setSlikaFile(base64Encoded);
			listaArtikalaDTO.add(artikalDTO);
		}
		return listaArtikalaDTO;
	}

	@Override
	public List<ProdavniceSaBrojemArtikalaDTO> findThreeBigMarket() {
		// TODO Auto-generated method stub
		List<Object[]> listaObjekata=prodavnicaRepository.findThreeBigestMarket();
		List<ProdavniceSaBrojemArtikalaDTO>listaVelicinaProdavnica= new ArrayList<ProdavniceSaBrojemArtikalaDTO>();
	
		int odVecegKaManjem=0;
		for(Object [] o: listaObjekata) {
			odVecegKaManjem++;
			int idProdavnica=Integer.parseInt(o[0].toString());
			int brojArtikala=Integer.parseInt(o[1].toString());
			ProdavniceSaBrojemArtikalaDTO prodavniceDto=new ProdavniceSaBrojemArtikalaDTO();
			prodavniceDto.setRedosledNajveciKaNajmanjem(odVecegKaManjem);
			prodavniceDto.setIdProdavnice(idProdavnica);
			prodavniceDto.setBrojArtikala(brojArtikala);
			listaVelicinaProdavnica.add(prodavniceDto);
			
		}
		
		return listaVelicinaProdavnica;
	}

}
