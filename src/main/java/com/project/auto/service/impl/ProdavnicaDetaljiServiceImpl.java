package com.project.auto.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import com.project.auto.dto.ProdavnicaDetalji;
import com.project.auto.model.Prodavnica;
import com.project.auto.repositoty.ProdavnicaRepository;

public class ProdavnicaDetaljiServiceImpl implements UserDetailsService {

	@Autowired 
	private ProdavnicaRepository prodavnicaRepository;
	
	@Override
	public UserDetails loadUserByUsername(String username) 
			throws UsernameNotFoundException {
		// TODO Auto-generated method stub
		System.out.println(username+" --> korisnicko ime");
		Prodavnica prodavnica=prodavnicaRepository.findByUsername(username);
		System.out.println(prodavnica.getIdProdavnica());
		if(prodavnica==null) {
			throw new UsernameNotFoundException("Ne mozemo da pronadjemo korisnika!");
		}
		return new ProdavnicaDetalji(prodavnica);
	}

}
