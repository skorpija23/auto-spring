package com.project.auto.service;

import java.io.UnsupportedEncodingException;
import java.util.List;

import com.project.auto.dto.ArtikalDTO;
import com.project.auto.dto.AutoMarkaDTO;
import com.project.auto.dto.AutoModelDTO;
import com.project.auto.dto.CreateArtikalDTO;
import com.project.auto.dto.CreateProdavnicaDTO;
import com.project.auto.dto.FilterArtiklaDTO;
import com.project.auto.dto.KategorijaDTO;
import com.project.auto.dto.PodKategorijaDTO;

import com.project.auto.dto.ProdavnicaDTO;
import com.project.auto.dto.ProdavniceSaBrojemArtikalaDTO;



public interface ProdavnicaService {

	void createNewUser(CreateProdavnicaDTO request);

	List<AutoModelDTO> findAllModel();

	List<AutoMarkaDTO> findAllAutoMarka();

	List<KategorijaDTO> findAllKategorija();

	List<PodKategorijaDTO> findAllPodKategorija();

	void saveNewArtikal(CreateArtikalDTO request);

	List<ProdavnicaDTO> findAllProdavnica(int brojStranice, int brojPrikaza);

	List<ArtikalDTO> filterArtikala(FilterArtiklaDTO request) throws UnsupportedEncodingException;

	List<ArtikalDTO> findArtikalById(int idProdavnica) throws UnsupportedEncodingException;

	List<ProdavniceSaBrojemArtikalaDTO> findThreeBigMarket();



}
