package com.project.auto.model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the pod_kategorija database table.
 * 
 */
@Entity
@Table(name="pod_kategorija")
@NamedQuery(name="PodKategorija.findAll", query="SELECT p FROM PodKategorija p")
public class PodKategorija implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id_pod_kategorija")
	private int idPodKategorija;

	private String naziv;

	private String opsi;

	//bi-directional many-to-one association to Artikal
	@OneToMany(mappedBy="podKategorija")
	private List<Artikal> artikals;

	//bi-directional many-to-one association to Kategorija
	@ManyToOne
	private Kategorija kategorija;

	public PodKategorija() {
	}

	public int getIdPodKategorija() {
		return this.idPodKategorija;
	}

	public void setIdPodKategorija(int idPodKategorija) {
		this.idPodKategorija = idPodKategorija;
	}

	public String getNaziv() {
		return this.naziv;
	}

	public void setNaziv(String naziv) {
		this.naziv = naziv;
	}

	public String getOpsi() {
		return this.opsi;
	}

	public void setOpsi(String opsi) {
		this.opsi = opsi;
	}

	public List<Artikal> getArtikals() {
		return this.artikals;
	}

	public void setArtikals(List<Artikal> artikals) {
		this.artikals = artikals;
	}

	public Artikal addArtikal(Artikal artikal) {
		getArtikals().add(artikal);
		artikal.setPodKategorija(this);

		return artikal;
	}

	public Artikal removeArtikal(Artikal artikal) {
		getArtikals().remove(artikal);
		artikal.setPodKategorija(null);

		return artikal;
	}

	public Kategorija getKategorija() {
		return this.kategorija;
	}

	public void setKategorija(Kategorija kategorija) {
		this.kategorija = kategorija;
	}

}