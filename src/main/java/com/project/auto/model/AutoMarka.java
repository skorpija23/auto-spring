package com.project.auto.model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the auto_marka database table.
 * 
 */
@Entity
@Table(name="auto_marka")
@NamedQuery(name="AutoMarka.findAll", query="SELECT a FROM AutoMarka a")
public class AutoMarka implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id_auto_marka")
	private int idAutoMarka;

	private String naziv;

	private String slika;

	//bi-directional many-to-one association to AutoModel
	@OneToMany(mappedBy="autoMarka")
	private List<AutoModel> autoModels;

	public AutoMarka() {
	}

	public int getIdAutoMarka() {
		return this.idAutoMarka;
	}

	public void setIdAutoMarka(int idAutoMarka) {
		this.idAutoMarka = idAutoMarka;
	}

	public String getNaziv() {
		return this.naziv;
	}

	public void setNaziv(String naziv) {
		this.naziv = naziv;
	}

	public String getSlika() {
		return this.slika;
	}

	public void setSlika(String slika) {
		this.slika = slika;
	}

	public List<AutoModel> getAutoModels() {
		return this.autoModels;
	}

	public void setAutoModels(List<AutoModel> autoModels) {
		this.autoModels = autoModels;
	}

	public AutoModel addAutoModel(AutoModel autoModel) {
		getAutoModels().add(autoModel);
		autoModel.setAutoMarka(this);

		return autoModel;
	}

	public AutoModel removeAutoModel(AutoModel autoModel) {
		getAutoModels().remove(autoModel);
		autoModel.setAutoMarka(null);

		return autoModel;
	}

}