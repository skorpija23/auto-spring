package com.project.auto.model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the prodavnica database table.
 * 
 */
@Entity
@NamedQuery(name="Prodavnica.findAll", query="SELECT p FROM Prodavnica p")
public class Prodavnica implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id_prodavnica")
	private int idProdavnica;

    private String naziv;
    
	private String adresa;

	@Column(name="email")
	private String email;

	@Lob
	@Column(name="o_nama")
	private String oNama;

	private String pass;
    
	private String pib;

	private String slika;

	private String telefon;

	private String username;
	

	//bi-directional many-to-one association to Artikal
	@OneToMany(mappedBy="prodavnica")
	private List<Artikal> artikals;


	public Prodavnica(int idProdavnica, String naziv, String adresa, String email, String oNama, String pass,
			String pib, String slika, String telefon, String username, List<Artikal> artikals) {
		super();
		this.idProdavnica = idProdavnica;
		this.naziv = naziv;
		this.adresa = adresa;
		this.email = email;
		this.oNama = oNama;
		this.pass = pass;
		this.pib = pib;
		this.slika = slika;
		this.telefon = telefon;
		this.username = username;
		this.artikals = artikals;
	}


	public Prodavnica() {
		super();
	}


	public int getIdProdavnica() {
		return idProdavnica;
	}


	public void setIdProdavnica(int idProdavnica) {
		this.idProdavnica = idProdavnica;
	}


	public String getNaziv() {
		return naziv;
	}


	public void setNaziv(String naziv) {
		this.naziv = naziv;
	}


	public String getAdresa() {
		return adresa;
	}


	public void setAdresa(String adresa) {
		this.adresa = adresa;
	}


	public String getEmail() {
		return email;
	}


	public void setEmail(String email) {
		this.email = email;
	}


	public String getoNama() {
		return oNama;
	}


	public void setoNama(String oNama) {
		this.oNama = oNama;
	}


	public String getPass() {
		return pass;
	}


	public void setPass(String pass) {
		this.pass = pass;
	}


	public String getPib() {
		return pib;
	}


	public void setPib(String pib) {
		this.pib = pib;
	}


	public String getSlika() {
		return slika;
	}


	public void setSlika(String slika) {
		this.slika = slika;
	}


	public String getTelefon() {
		return telefon;
	}


	public void setTelefon(String telefon) {
		this.telefon = telefon;
	}


	public String getUsername() {
		return username;
	}


	public void setUsername(String username) {
		this.username = username;
	}


	public List<Artikal> getArtikals() {
		return artikals;
	}


	public void setArtikals(List<Artikal> artikals) {
		this.artikals = artikals;
	}


	public static long getSerialversionuid() {
		return serialVersionUID;
	}


	@Override
	public String toString() {
		return "Prodavnica [idProdavnica=" + idProdavnica + ", naziv=" + naziv + ", adresa=" + adresa + ", email="
				+ email + ", oNama=" + oNama + ", pass=" + pass + ", pib=" + pib + ", slika=" + slika + ", telefon="
				+ telefon + ", username=" + username + "]";
	}



	
}