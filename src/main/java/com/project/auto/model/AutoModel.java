package com.project.auto.model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the auto_model database table.
 * 
 */
@Entity
@Table(name="auto_model")
@NamedQuery(name="AutoModel.findAll", query="SELECT a FROM AutoModel a")
public class AutoModel implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id_auto_model")
	private int idAutoModel;

	private String godiste;

	private String naziv;

	private String oznaka;

	private String slika;

	//bi-directional many-to-one association to Artikal
	@OneToMany(mappedBy="autoModel")
	private List<Artikal> artikals;

	//bi-directional many-to-one association to AutoMarka
	@ManyToOne
	@JoinColumn(name="auto_marka_id_auto_marka")
	private AutoMarka autoMarka;

	public AutoModel() {
	}

	public int getIdAutoModel() {
		return this.idAutoModel;
	}

	public void setIdAutoModel(int idAutoModel) {
		this.idAutoModel = idAutoModel;
	}

	public String getGodiste() {
		return this.godiste;
	}

	public void setGodiste(String godiste) {
		this.godiste = godiste;
	}

	public String getNaziv() {
		return this.naziv;
	}

	public void setNaziv(String naziv) {
		this.naziv = naziv;
	}

	public String getOznaka() {
		return this.oznaka;
	}

	public void setOznaka(String oznaka) {
		this.oznaka = oznaka;
	}

	public String getSlika() {
		return this.slika;
	}

	public void setSlika(String slika) {
		this.slika = slika;
	}

	public List<Artikal> getArtikals() {
		return this.artikals;
	}

	public void setArtikals(List<Artikal> artikals) {
		this.artikals = artikals;
	}

	public Artikal addArtikal(Artikal artikal) {
		getArtikals().add(artikal);
		artikal.setAutoModel(this);

		return artikal;
	}

	public Artikal removeArtikal(Artikal artikal) {
		getArtikals().remove(artikal);
		artikal.setAutoModel(null);

		return artikal;
	}

	public AutoMarka getAutoMarka() {
		return this.autoMarka;
	}

	public void setAutoMarka(AutoMarka autoMarka) {
		this.autoMarka = autoMarka;
	}

}