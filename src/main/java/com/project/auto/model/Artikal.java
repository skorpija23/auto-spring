package com.project.auto.model;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the artikal database table.
 * 
 */
@Entity
@NamedQuery(name="Artikal.findAll", query="SELECT a FROM Artikal a")
public class Artikal implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id_artikal")
	private int idArtikal;

	private String naziv;
	
	private String slika;

	@Lob	
	private String opis;

	private float cena;

	private float pdv;
	@Column(name="cena_pdv")
	
	private float cenaPdv;

	
	@Lob
	@Column(name="slika_file")
	private byte[] slikaFile;



	//bi-directional many-to-one association to Prodavnica
	@ManyToOne
	private Prodavnica prodavnica;

	//bi-directional many-to-one association to AutoModel
	@ManyToOne
	@JoinColumn(name="auto_model_id_auto_model")
	private AutoModel autoModel;

	//bi-directional many-to-one association to PodKategorija
	@ManyToOne
	@JoinColumn(name="pod_kategorija_id_pod_kategorija")
	private PodKategorija podKategorija;

	public Artikal() {
	}
	

	public float getCena() {
		return this.cena;
	}

	public void setCena(float cena) {
		this.cena = cena;
	}

	public float getCenaPdv() {
		return this.cenaPdv;
	}

	public void setCenaPdv(float cenaPdv) {
		this.cenaPdv = cenaPdv;
	}

	public int getIdArtikal() {
		return this.idArtikal;
	}

	public void setIdArtikal(int idArtikal) {
		this.idArtikal = idArtikal;
	}



	public String getNaziv() {
		return this.naziv;
	}

	public void setNaziv(String naziv) {
		this.naziv = naziv;
	}

	public String getOpis() {
		return this.opis;
	}

	public void setOpis(String opis) {
		this.opis = opis;
	}

	public float getPdv() {
		return this.pdv;
	}

	public void setPdv(float pdv) {
		this.pdv = pdv;
	}

	public String getSlika() {
		return this.slika;
	}

	public void setSlika(String slika) {
		this.slika = slika;
	}

	public byte[] getSlikaFile() {
		return this.slikaFile;
	}

	public void setSlikaFile(byte[] slikaFile) {
		this.slikaFile = slikaFile;
	}


	public Prodavnica getProdavnica() {
		return this.prodavnica;
	}

	public void setProdavnica(Prodavnica prodavnica) {
		this.prodavnica = prodavnica;
	}

	public AutoModel getAutoModel() {
		return this.autoModel;
	}

	public void setAutoModel(AutoModel autoModel) {
		this.autoModel = autoModel;
	}

	public PodKategorija getPodKategorija() {
		return this.podKategorija;
	}

	public void setPodKategorija(PodKategorija podKategorija) {
		this.podKategorija = podKategorija;
	}

}