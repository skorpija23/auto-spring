package com.project.auto.model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the kategorija database table.
 * 
 */
@Entity
@NamedQuery(name="Kategorija.findAll", query="SELECT k FROM Kategorija k")
public class Kategorija implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id_kategorija")
	private int idKategorija;

	private String naziv;


	private String opis;

	private String slika;

	//bi-directional many-to-one association to PodKategorija
	@OneToMany(mappedBy="kategorija")
	private List<PodKategorija> podKategorijas;

	public Kategorija() {
	}

	public int getIdKategorija() {
		return this.idKategorija;
	}

	public void setIdKategorija(int idKategorija) {
		this.idKategorija = idKategorija;
	}

	public String getNaziv() {
		return this.naziv;
	}

	public void setNaziv(String naziv) {
		this.naziv = naziv;
	}


	public String getOpis() {
		return this.opis;
	}

	public void setOpis(String opis) {
		this.opis = opis;
	}

	public String getSlika() {
		return this.slika;
	}

	public void setSlika(String slika) {
		this.slika = slika;
	}

	public List<PodKategorija> getPodKategorijas() {
		return this.podKategorijas;
	}

	public void setPodKategorijas(List<PodKategorija> podKategorijas) {
		this.podKategorijas = podKategorijas;
	}

	public PodKategorija addPodKategorija(PodKategorija podKategorija) {
		getPodKategorijas().add(podKategorija);
		podKategorija.setKategorija(this);

		return podKategorija;
	}

	public PodKategorija removePodKategorija(PodKategorija podKategorija) {
		getPodKategorijas().remove(podKategorija);
		podKategorija.setKategorija(null);

		return podKategorija;
	}

}