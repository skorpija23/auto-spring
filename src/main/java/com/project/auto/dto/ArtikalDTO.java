package com.project.auto.dto;

import java.util.Arrays;

import javax.persistence.Column;
import javax.persistence.Lob;

public class ArtikalDTO {

    private String naziv;
	
	private String slika;

	private String opis;

	private float cena;

	private float pdv;
	
	private float cenaPdv;

	private String slikaFile;

	public ArtikalDTO(String naziv, String slika, String opis, float cena, float pdv, float cenaPdv, String slikaFile) {
		super();
		this.naziv = naziv;
		this.slika = slika;
		this.opis = opis;
		this.cena = cena;
		this.pdv = pdv;
		this.cenaPdv = cenaPdv;
		this.slikaFile = slikaFile;
	}

	public ArtikalDTO() {
		super();
	}

	public String getNaziv() {
		return naziv;
	}

	public void setNaziv(String naziv) {
		this.naziv = naziv;
	}

	public String getSlika() {
		return slika;
	}

	public void setSlika(String slika) {
		this.slika = slika;
	}

	public String getOpis() {
		return opis;
	}

	public void setOpis(String opis) {
		this.opis = opis;
	}

	public float getCena() {
		return cena;
	}

	public void setCena(float cena) {
		this.cena = cena;
	}

	public float getPdv() {
		return pdv;
	}

	public void setPdv(float pdv) {
		this.pdv = pdv;
	}

	public float getCenaPdv() {
		return cenaPdv;
	}

	public void setCenaPdv(float cenaPdv) {
		this.cenaPdv = cenaPdv;
	}

	public String getSlikaFile() {
		return slikaFile;
	}

	public void setSlikaFile(String slikaFile) {
		this.slikaFile = slikaFile;
	}

	@Override
	public String toString() {
		return "ArtikalDTO [naziv=" + naziv + ", slika=" + slika + ", opis=" + opis + ", cena=" + cena + ", pdv=" + pdv
				+ ", cenaPdv=" + cenaPdv + ", slikaFile=" + slikaFile + "]";
	}

	

	
	
}
