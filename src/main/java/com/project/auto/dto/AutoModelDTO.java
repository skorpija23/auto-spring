package com.project.auto.dto;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

public class AutoModelDTO {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id_auto_model")
	private int idAutoModel;

	private String godiste;

	private String naziv;

	private String oznaka;

	private String slika;

	public AutoModelDTO(int idAutoModel, String godiste, String naziv, String oznaka, String slika) {
		super();
		this.idAutoModel = idAutoModel;
		this.godiste = godiste;
		this.naziv = naziv;
		this.oznaka = oznaka;
		this.slika = slika;
	}

	public AutoModelDTO() {
		super();
	}

	public int getIdAutoModel() {
		return idAutoModel;
	}

	public void setIdAutoModel(int idAutoModel) {
		this.idAutoModel = idAutoModel;
	}

	public String getGodiste() {
		return godiste;
	}

	public void setGodiste(String godiste) {
		this.godiste = godiste;
	}

	public String getNaziv() {
		return naziv;
	}

	public void setNaziv(String naziv) {
		this.naziv = naziv;
	}

	public String getOznaka() {
		return oznaka;
	}

	public void setOznaka(String oznaka) {
		this.oznaka = oznaka;
	}

	public String getSlika() {
		return slika;
	}

	public void setSlika(String slika) {
		this.slika = slika;
	}

	@Override
	public String toString() {
		return "AutoModelDTO [idAutoModel=" + idAutoModel + ", godiste=" + godiste + ", naziv=" + naziv + ", oznaka="
				+ oznaka + ", slika=" + slika + "]";
	}
	
	
}
