package com.project.auto.dto;

public class AutoMarkaDTO {


	private int idAutoMarka;

	private String naziv;

	private String slika;

	public AutoMarkaDTO(int idAutoMarka, String naziv, String slika) {
		super();
		this.idAutoMarka = idAutoMarka;
		this.naziv = naziv;
		this.slika = slika;
	}

	public AutoMarkaDTO() {
		super();
	}

	public int getIdAutoMarka() {
		return idAutoMarka;
	}

	public void setIdAutoMarka(int idAutoMarka) {
		this.idAutoMarka = idAutoMarka;
	}

	public String getNaziv() {
		return naziv;
	}

	public void setNaziv(String naziv) {
		this.naziv = naziv;
	}

	public String getSlika() {
		return slika;
	}

	public void setSlika(String slika) {
		this.slika = slika;
	}

	@Override
	public String toString() {
		return "AutoMarkaDTO [idAutoMarka=" + idAutoMarka + ", naziv=" + naziv + ", slika=" + slika + "]";
	}
	
	
	
}
