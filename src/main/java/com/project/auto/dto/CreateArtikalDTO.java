package com.project.auto.dto;

import java.util.Arrays;



public class CreateArtikalDTO {

    private int idAutoModel;
    private int idAutoMarka;
    private int idKategorija;
    private int idPodKategorija;
    private String username ;
	private String naziv;	
	private String slika;
	private String opis;
	private float cena;
	private float pdv;	
	private float cenaPdv;
	private byte[] slikaFile;
	public CreateArtikalDTO(int idAutoModel, int idAutoMarka, int idKategorija, int idPodKategorija, String username,
			String naziv, String slika, String opis, float cena, float pdv, float cenaPdv, byte[] slikaFile) {
		super();
		this.idAutoModel = idAutoModel;
		this.idAutoMarka = idAutoMarka;
		this.idKategorija = idKategorija;
		this.idPodKategorija = idPodKategorija;
		this.username = username;
		this.naziv = naziv;
		this.slika = slika;
		this.opis = opis;
		this.cena = cena;
		this.pdv = pdv;
		this.cenaPdv = cenaPdv;
		this.slikaFile = slikaFile;
	}
	public CreateArtikalDTO() {
		super();
	}
	public int getIdAutoModel() {
		return idAutoModel;
	}
	public void setIdAutoModel(int idAutoModel) {
		this.idAutoModel = idAutoModel;
	}
	public int getIdAutoMarka() {
		return idAutoMarka;
	}
	public void setIdAutoMarka(int idAutoMarka) {
		this.idAutoMarka = idAutoMarka;
	}
	public int getIdKategorija() {
		return idKategorija;
	}
	public void setIdKategorija(int idKategorija) {
		this.idKategorija = idKategorija;
	}
	public int getIdPodKategorija() {
		return idPodKategorija;
	}
	public void setIdPodKategorija(int idPodKategorija) {
		this.idPodKategorija = idPodKategorija;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String Username) {
		this.username = Username;
	}
	public String getNaziv() {
		return naziv;
	}
	public void setNaziv(String naziv) {
		this.naziv = naziv;
	}
	public String getSlika() {
		return slika;
	}
	public void setSlika(String slika) {
		this.slika = slika;
	}
	public String getOpis() {
		return opis;
	}
	public void setOpis(String opis) {
		this.opis = opis;
	}
	public float getCena() {
		return cena;
	}
	public void setCena(float cena) {
		this.cena = cena;
	}
	public float getPdv() {
		return pdv;
	}
	public void setPdv(float pdv) {
		this.pdv = pdv;
	}
	public float getCenaPdv() {
		return cenaPdv;
	}
	public void setCenaPdv(float cenaPdv) {
		this.cenaPdv = cenaPdv;
	}
	public byte[] getSlikaFile() {
		return slikaFile;
	}
	public void setSlikaFile(byte[] slikaFile) {
		this.slikaFile = slikaFile;
	}
	@Override
	public String toString() {
		return "CreateArtikalDTO [idAutoModel=" + idAutoModel + ", idAutoMarka=" + idAutoMarka + ", idKategorija="
				+ idKategorija + ", idPodKategorija=" + idPodKategorija + ", username=" + username + ", naziv="
				+ naziv + ", slika=" + slika + ", opis=" + opis + ", cena=" + cena + ", pdv=" + pdv + ", cenaPdv="
				+ cenaPdv + ", slikaFile=" + Arrays.toString(slikaFile) + "]";
	}


}
