package com.project.auto.dto;

public class FilterArtiklaDTO {

	    private int idAutoModel;
	    private int idPodKategorija;
		private String naziv;
		public FilterArtiklaDTO(int idAutoModel, int idPodKategorija, String naziv) {
			super();
			this.idAutoModel = idAutoModel;
			this.idPodKategorija = idPodKategorija;
			this.naziv = naziv;
		}
		public FilterArtiklaDTO() {
			super();
		}
		public int getIdAutoModel() {
			return idAutoModel;
		}
		public void setIdAutoModel(int idAutoModel) {
			this.idAutoModel = idAutoModel;
		}
		public int getIdPodKategorija() {
			return idPodKategorija;
		}
		public void setIdPodKategorija(int idPodKategorija) {
			this.idPodKategorija = idPodKategorija;
		}
		public String getNaziv() {
			return naziv;
		}
		public void setNaziv(String naziv) {
			this.naziv = naziv;
		}
		@Override
		public String toString() {
			return "FilterArtiklaDTO [idAutoModel=" + idAutoModel + ", idPodKategorija=" + idPodKategorija + ", naziv="
					+ naziv + "]";
		}
		
		
		
		
}
