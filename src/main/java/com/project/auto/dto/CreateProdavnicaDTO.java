package com.project.auto.dto;



public class CreateProdavnicaDTO {

	 private String naziv;
	    
		private String adresa;

		private String email;
	
		private String oNama;

		private String pass;
	    
		private String pib;

		private String slika;

		private String telefon;

		private String username;

		public CreateProdavnicaDTO(String naziv, String adresa, String email, String oNama, String pass, String pib,
				String slika, String telefon, String username) {
			super();
			this.naziv = naziv;
			this.adresa = adresa;
			this.email = email;
			this.oNama = oNama;
			this.pass = pass;
			this.pib = pib;
			this.slika = slika;
			this.telefon = telefon;
			this.username = username;
		}

		public CreateProdavnicaDTO() {
			super();
		}

		public String getNaziv() {
			return naziv;
		}

		public void setNaziv(String naziv) {
			this.naziv = naziv;
		}

		public String getAdresa() {
			return adresa;
		}

		public void setAdresa(String adresa) {
			this.adresa = adresa;
		}

		public String getEmail() {
			return email;
		}

		public void setEmail(String email) {
			this.email = email;
		}

		public String getoNama() {
			return oNama;
		}

		public void setoNama(String oNama) {
			this.oNama = oNama;
		}

		public String getPass() {
			return pass;
		}

		public void setPass(String pass) {
			this.pass = pass;
		}

		public String getPib() {
			return pib;
		}

		public void setPib(String pib) {
			this.pib = pib;
		}

		public String getSlika() {
			return slika;
		}

		public void setSlika(String slika) {
			this.slika = slika;
		}

		public String getTelefon() {
			return telefon;
		}

		public void setTelefon(String telefon) {
			this.telefon = telefon;
		}

		public String getUsername() {
			return username;
		}

		public void setUsername(String username) {
			this.username = username;
		}

		@Override
		public String toString() {
			return "CreateProdavnicaDTO [naziv=" + naziv + ", adresa=" + adresa + ", email=" + email + ", oNama="
					+ oNama + ", pass=" + pass + ", pib=" + pib + ", slika=" + slika + ", telefon=" + telefon
					+ ", username=" + username + "]";
		}
		
		
}
