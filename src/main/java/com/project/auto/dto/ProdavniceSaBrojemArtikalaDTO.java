package com.project.auto.dto;

public class ProdavniceSaBrojemArtikalaDTO {

	private int idProdavnice;
	private int brojArtikala;
	private int redosledNajveciKaNajmanjem;
	public ProdavniceSaBrojemArtikalaDTO(int idProdavnice, int brojArtikala, int redosledNajveciKaNajmanjem) {
		super();
		this.idProdavnice = idProdavnice;
		this.brojArtikala = brojArtikala;
		this.redosledNajveciKaNajmanjem = redosledNajveciKaNajmanjem;
	}
	public ProdavniceSaBrojemArtikalaDTO() {
		super();
	}
	public int getIdProdavnice() {
		return idProdavnice;
	}
	public void setIdProdavnice(int idProdavnice) {
		this.idProdavnice = idProdavnice;
	}
	public int getBrojArtikala() {
		return brojArtikala;
	}
	public void setBrojArtikala(int brojArtikala) {
		this.brojArtikala = brojArtikala;
	}
	public int getRedosledNajveciKaNajmanjem() {
		return redosledNajveciKaNajmanjem;
	}
	public void setRedosledNajveciKaNajmanjem(int redosledNajveciKaNajmanjem) {
		this.redosledNajveciKaNajmanjem = redosledNajveciKaNajmanjem;
	}
	@Override
	public String toString() {
		return "ProdavniceSaBrojemArtikalaDTO [idProdavnice=" + idProdavnice + ", brojArtikala=" + brojArtikala
				+ ", redosledNajveciKaNajmanjem=" + redosledNajveciKaNajmanjem + "]";
	}
	
	
}
