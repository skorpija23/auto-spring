package com.project.auto.dto;


public class PodKategorijaDTO {

	
	private int idPodKategorija;

	private String naziv;

	private String opsi;

	public PodKategorijaDTO(int idPodKategorija, String naziv, String opsi) {
		super();
		this.idPodKategorija = idPodKategorija;
		this.naziv = naziv;
		this.opsi = opsi;
	}

	public PodKategorijaDTO() {
		super();
	}

	public int getIdPodKategorija() {
		return idPodKategorija;
	}

	public void setIdPodKategorija(int idPodKategorija) {
		this.idPodKategorija = idPodKategorija;
	}

	public String getNaziv() {
		return naziv;
	}

	public void setNaziv(String naziv) {
		this.naziv = naziv;
	}

	public String getOpsi() {
		return opsi;
	}

	public void setOpsi(String opsi) {
		this.opsi = opsi;
	}

	@Override
	public String toString() {
		return "PodKategorijaDTO [idPodKategorija=" + idPodKategorija + ", naziv=" + naziv + ", opsi=" + opsi + "]";
	}
	
	
}
