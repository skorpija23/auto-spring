package com.project.auto.dto;



public class KategorijaDTO {

	private int idKategorija;

	private String naziv;

	private String opis;

	private String slika;

	public KategorijaDTO(int idKategorija, String naziv, String nazivKategorije, String opis, String slika) {
		super();
		this.idKategorija = idKategorija;
		this.naziv = naziv;
		this.opis = opis;
		this.slika = slika;
	}

	public KategorijaDTO() {
		super();
	}

	public int getIdKategorija() {
		return idKategorija;
	}

	public void setIdKategorija(int idKategorija) {
		this.idKategorija = idKategorija;
	}

	public String getNaziv() {
		return naziv;
	}

	public void setNaziv(String naziv) {
		this.naziv = naziv;
	}

	public String getOpis() {
		return opis;
	}

	public void setOpis(String opis) {
		this.opis = opis;
	}

	public String getSlika() {
		return slika;
	}

	public void setSlika(String slika) {
		this.slika = slika;
	}

	@Override
	public String toString() {
		return "KategorijaDTO [idKategorija=" + idKategorija + ", naziv=" + naziv + ", opis=" + opis + ", slika="
				+ slika + "]";
	}



	
}
