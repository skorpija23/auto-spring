package com.project.auto.dto;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Lob;
import javax.persistence.OneToMany;

import com.project.auto.model.Artikal;
import com.project.auto.model.Prodavnica;

public class ProdavnicaDTO {

	private List<Prodavnica> listaProdavnica;

	private int ukupanBrojStranica;

	public ProdavnicaDTO(List<Prodavnica> listaProdavnica, int ukupanBrojStranica) {
		super();
		this.listaProdavnica = listaProdavnica;
		this.ukupanBrojStranica = ukupanBrojStranica;
	}

	public ProdavnicaDTO() {
		super();
	}

	public List<Prodavnica> getListaProdavnica() {
		return listaProdavnica;
	}

	public void setListaProdavnica(List<Prodavnica> listaProdavnica) {
		this.listaProdavnica = listaProdavnica;
	}

	public int getUkupanBrojStranica() {
		return ukupanBrojStranica;
	}

	public void setUkupanBrojStranica(int ukupanBrojStranica) {
		this.ukupanBrojStranica = ukupanBrojStranica;
	}

	
	
}
